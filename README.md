# Trae Brown

## Software Engineering Student: Ambitious Junior Undergraduate With a Passion For Creating

## Hobbies and Interests
#### Reading
#### Writing
#### Watching NFL/College Football
#### Playing Golf
#### Working Out

## Experience
### Web/Mobile Development
#### MERN Stack
#### Swift/Flutter/React
#### Internships @ Gallup, Microsoft, Autodesk
#### Morehouse CS Tutor/Lab Assistant
